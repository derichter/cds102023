using AdminService as service from '../../srv/admin-service';

annotate service.Bookings with @(
    Capabilities.InsertRestrictions: {Insertable: true, },
    UI.HeaderInfo                  : {
        TypeName      : 'Booking',
        TypeNamePlural: 'Bookings',
        Title         : {
            $Type: 'UI.DataField',
            Value: 'Flight booking',
        },
        Description   : {
            $Type: 'UI.DataField',
            Value: customer.name,
        },
    },
    UI.Identification              : [{
        $Type             : 'UI.DataFieldForAction',
        Label             : 'Set is smoker',
        Action            : 'AdminService.setIsSmoker',
        ![@UI.Importance] : #High
    }],
    UI.LineItem                    : [
        {
            $Type: 'UI.DataField',
            Label: 'Customer',
            Value: customer.name,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Is smoker',
            Value: smoker,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Class',
            Value: class.descr,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Luggage weight',
            Value: luggweigh,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Foreign amount',
            Value: forcuram,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Local amount',
            Value: loccuram,
        },
    ],
    UI.FieldGroup #GeneratedGroup1 : {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Customer',
                Value: customer.name,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Is smoker',
                Value: smoker,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Class',
                Value: class.descr,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Luggage weight',
                Value: luggweigh,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Foreign amount',
                Value: forcuram,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Local amount',
                Value: loccuram,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Order date',
                Value: order_dat,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Cancelled',
                Value: cancelled,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Reserved',
                Value: reserved,
            },
        ],
    },
    UI.Facets                      : [{
        $Type : 'UI.ReferenceFacet',
        ID    : 'GeneratedFacet1',
        Label : 'General Information',
        Target: '@UI.FieldGroup#GeneratedGroup1',
    }, ]
);
