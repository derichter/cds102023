using AdminService as service from '../../srv/admin-service';

annotate service.Flights with @(
    odata.draft.enabled            : true,
    Capabilities.InsertRestrictions: {Insertable: true, },
    UI.HeaderInfo                  : {
        TypeName      : 'Flight',
        TypeNamePlural: 'Flights',
        Title         : {
            $Type: 'UI.DataField',
            Value: 'Flight',
        },
        Description   : {
            $Type: 'UI.DataField',
            Value: fldate,
        },
    },
    UI.LineItem                    : [
        {
            $Type: 'UI.DataField',
            Label: 'Carrier',
            Value: connection.carrier_ID,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Connection',
            Value: connection_ID,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Flight date',
            Value: fldate,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Price',
            Value: price,
        },
        {
            $Type: 'UI.DataField',
            Label: 'Plane type',
            Value: planetype,
        },
    ],
    UI.FieldGroup #GeneratedGroup1 : {
        $Type: 'UI.FieldGroupType',
        Data : [
            {
                $Type: 'UI.DataField',
                Label: 'Carrier',
                Value: connection.carrier_ID,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Connection',
                Value: connection_ID,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Flight date',
                Value: fldate,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Price',
                Value: price,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Plane type',
                Value: planetype,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Payment sum',
                Value: paymentsum,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Seats max',
                Value: seatsmax,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Seats occupied',
                Value: seatsocc,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Business seats max',
                Value: seatsmax_b,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Business seats occupied',
                Value: seatsocc_b,
            },
            {
                $Type: 'UI.DataField',
                Label: 'First class seats max',
                Value: seatsmax_f,
            },
            {
                $Type: 'UI.DataField',
                Label: 'First class seats occupied',
                Value: seatsocc_f,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Economy seats max',
                Value: seatsmax_e,
            },
            {
                $Type: 'UI.DataField',
                Label: 'Economy seats occupied',
                Value: seatsocc_e,
            },
        ],
    },
    UI.Facets                      : [
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'GeneratedFacet1',
            Label : 'General Information',
            Target: '@UI.FieldGroup#GeneratedGroup1',
        },
        {
            $Type : 'UI.ReferenceFacet',
            ID    : 'GeneratedFacet2',
            Label : 'Bookings',
            Target: 'bookings/@UI.LineItem',
        },
    ]
);
