sap.ui.define(
    ["sap/fe/core/AppComponent"],
    function (Component) {
        "use strict";

        return Component.extend("flj.demo.app.Component", {
            metadata: {
                manifest: "json"
            }
        });
    }
);