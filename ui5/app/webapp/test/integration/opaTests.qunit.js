sap.ui.require(
    [
        'sap/fe/test/JourneyRunner',
        'flj/demo/app/test/integration/FirstJourney',
		'flj/demo/app/test/integration/pages/FlightsList',
		'flj/demo/app/test/integration/pages/FlightsObjectPage',
		'flj/demo/app/test/integration/pages/BookingsObjectPage'
    ],
    function(JourneyRunner, opaJourney, FlightsList, FlightsObjectPage, BookingsObjectPage) {
        'use strict';
        var JourneyRunner = new JourneyRunner({
            // start index.html in web folder
            launchUrl: sap.ui.require.toUrl('flj/demo/app') + '/index.html'
        });

       
        JourneyRunner.run(
            {
                pages: { 
					onTheFlightsList: FlightsList,
					onTheFlightsObjectPage: FlightsObjectPage,
					onTheBookingsObjectPage: BookingsObjectPage
                }
            },
            opaJourney.run
        );
    }
);