# SAP Core Data Services (CDS) 

https://cap.cloud.sap/docs/get-started

Jumpstart a project:
```
cds init
cds watch
```

## CDS Entity

- Generates a Table
- Definition of the table scheme

```
entity Flights {...}
```

## CDS View

- Generates a View
- Select from one or more tables

```
entity Flights as select from my.Flights
```

## Projection View

- View that projects only one table
- Used in service definitions

```
entity Flights as projection on my.Flights
```

## Extensions
- Adds fields to an existing entity
```
extend Flights with {
    seatsmax   : Integer;
    seatsocc   : Integer;
    seatsmax_b : Integer;
    seatsocc_b : Integer;
    seatsmax_f : Integer;
    seatsocc_f : Integer;
}
```

## Virtual fields
- Fields that are not persistent in the database 
- They exist only in the entity and can be calculated by read-handler
```
...
virtual seatsmax_e : Integer;
virtual seatsocc_e : Integer;
...
```

## Types / Aspects

- Types = "Data elements / Structure types" in ABAP
- Aspects = "Inlcude structures" in ABAP

https://cap.cloud.sap/docs/cds/common

### Common Types:
```
type Language : Association to sap.common.Languages;
type Currency : Association to sap.common.Currencies;
type Country : Association to sap.common.Countries;
```

```
entity Languages : CodeList {
    key code : Locale;
}
entity Countries : CodeList {
    key code : String(3) @(title : '{i18n>CountryCode}');
}
entity Currencies : CodeList {
    key code      : String(3) @(title : '{i18n>CurrencyCode}');
        symbol    : String(5) @(title : '{i18n>CurrencySymbol}');
        minorUnit : Int16     @(title : '{i18n>CurrencyMinorUnit}');
}
```

### Common Aspects:
```
aspect cuid {
  key ID : UUID; //> automatically filled in
}
aspect managed {
    createdAt  : Timestamp @cds.on.insert : $now;
    createdBy  : User      @cds.on.insert : $user;
    modifiedAt : Timestamp @cds.on.insert : $now  @cds.on.update : $now;
    modifiedBy : User      @cds.on.insert : $user @cds.on.update : $user;
}
aspect temporal {
    validFrom : Timestamp @cds.valid.from;
    validTo   : Timestamp @cds.valid.to;
}
```

## Associations

- Can be ```Association to one``` or ```Association to many```
- ```Association to``` is implicitly ```to one``` 

### Unmanaged Association
Explicit on condition
```
entity Flights ... {
    connection_ID : Connid;
    connection    : Association to one Connections
                        on connection.connection_ID = connection_ID;
...
```

### Managed Association
Implicit on condition plus the field ``flight_ID`` is generated
```
entity Bookings ... {
    flight    : Association to one Flights;
...
```

### Composition
Like association, but with additional information about the hierarchy placement
```
entity Bookings ... {
    flight    : Composition of Flights;
...
```

### To many Association
Simple on-condition for bidirectional associations. ```$self``` refers to the enitity itself
```
entity Flights ... {
    bookings      : Association to many Bookings
                        on bookings.flight = $self;
...
```

## Localization
The CDS framework provides a simple way to handle translations for the data. Adding the keyword ```localized``` to a field automatically creates a text table. Which translation is displayed is also automatically handled by the framework depending on the language of the logged-in user
```
type Class        : Association to ClassTypes;
entity ClassTypes {
    key code  : String(1);
        name  : localized String(255);
        descr : localized String(1000);
}
```
In addition to the CSV file for the plain table (e.g. ```ClassTypes.csv```), you can also specify the mocked values of the text table in a CSV file (e.g. ```ClassTypes.texts.csv```)

## Testing

```GET```-requests can be tested using the browser

![http request](./http_request.png)

Other requests like ```POST``` etc. can be tested using a ```test/requests.http``` file
```
@service = http://localhost:4004/odata/v4/my
...
### ------------------------------------------------------------------------
POST {{service}}/Bookings
Content-Type: application/json

{
    "ID": "bd93ed26-fb5e-48dc-8a6a-520611d0ae95",
    "flight_ID": "76e59ff2-6494-41c3-8c9f-9d555fb171d2",
    "customer_ID": "d649e45e-3e1a-46b4-94b7-5769e74275e2",
    ...
}
```

## Custom Service Logic / Hooks

Generic CRUD operations are provided by the CDS framework and work out-of-the-box. 

https://cap.cloud.sap/docs/guides/providing-services#custom-logic

![custom logic](https://cap.cloud.sap/docs/assets/service-centric-paradigm.drawio.8bbbc720.svg)

Custom event handlers can be added by using the so-called "Hooks" for an entity ```before```, ```on``` and  ```after```

```
const cds = require('@sap/cds')

module.exports = function (){  
    this.on('CREATE', `Bookings`, async (req, next) => {...})
    this.on('READ', `Flights`, async (req, next) => {...})
    ...
}
```

These event handlers are defined in a Javascript file (e.g. ```service.js```), which must match the name of the service file (e.g. ```service.cds```).

## Query Language (CQL) / Query Notation (CQN)

The CDS framework provides a Javascript embedded query language (CQL) based on standard SQL

https://cap.cloud.sap/docs/cds/cqn

There are several ways to execute SQL statements in Javascript using the CDS utilities. However, in the end they are all compiled into a CQN object, which is then used to execute the query

```
# cql statement
cds.parse.cql(`SELECT from MyService.Flights`)

# query object
SELECT.from(`MyService.Flights`)

# cqn object
SELECT: {from: {ref: ['MyService.Flights']}} 
```

## Authorization

Access to the entire service or even to individual entities can be restricted by specifying the role that the requester must have

```
service UserService @(requires: 'authenticated-user') {...}
...
service AdminService @(requires: 'admin') {...}
```

CDS offers predefined mock user for testing
```
"users": {
    "alice": { "tenant": "t1", "roles": [ "cds.Subscriber", "admin" ] },
    "bob":   { "tenant": "t1", "roles": [ "cds.ExtensionDeveloper", "cds.UIFlexDeveloper" ] },
    ...
}
```

You can use mock user in the ```test/requests.http``` file like this
```
...
### ------------------------------------------------------------------------
GET {{service}}/user/Flights
Authorization: Basic user:
...
### ------------------------------------------------------------------------
POST {{service}}/admin/Bookings(00f4f4a4-e039-4e14-b4e6-31b525ab91ac)/setIsSmoker
Authorization: Basic alice:
Content-Type: application/json

{
    "smoker": false
}
```

https://cap.cloud.sap/docs/node.js/authentication#mock-users

## Read-only / Mandatory

Another way to restrict access to the database are the annotations ```@readonly``` and ```@mandatory```
```
@readonly
entity Flights as projection on core.Flights;
```

Updating a read-only entity results in an HTTP error
```
HTTP/1.1 405 Method Not Allowed
...
{
  "error": {
    "code": "405",
    "message": "Entity \"Flights\" is not updatable",
    "@Common.numericSeverity": 4
  }
}
```

However, only entities can be specified as read-only (e.g. actions can bypass this)

```
entity Bookings : cuid {
    customer  : Association to Customers @mandatory;
    flight    : Association to Flights   @mandatory;
    ...
}
```

Creating an entity with missing mandatory fields will also results in an HTTP error
```
HTTP/1.1 400 Bad Request
...
{
  "error": {
    "code": "400",
    "message": "Value is required",
    "target": "customer_ID",
    "@Common.numericSeverity": 4
  }
}
```

## Annotations (and outlook on Fiori-Elements)

Annotations provide meta information for the entities and are defined by a preceding ```@```-symbol
```
name  : localized String(255)  @title: '{i18n>Name}';
descr : localized String(1000) @title: '{i18n>Description}';
```
They can be used e.g to display labels for the fields or even to create entire elements of the app
```
using AdminService as service from '../../srv/admin-service';

annotate service.Flights with @(
    UI.HeaderInfo                  : {
        TypeName      : 'Flight',
        TypeNamePlural: 'Flights',
        Title         : {
            $Type: 'UI.DataField',
            Value: 'Flight',
        },
        Description   : {
            $Type: 'UI.DataField',
            Value: fldate,
        },
    },
    UI.LineItem                    : [
        {
            $Type: 'UI.DataField',
            Label: 'Carrier',
            Value: connection.carrier_ID,
        },
        ...
    }
) 
```

To jumpstart app development with Fiori-Elements, use the build-in wizard under ```File > New Project From Template```

![fe wizard](./fe_wizard.png)

If you use the local CAP project as data source, the wizard generates a application with some annotations like the one above. And the generated application is then available on the test page of the service

![test page](./test_page.png)