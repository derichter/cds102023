using {
    cuid,
    Country,
    Currency,
    Language,
    sap.common.CodeList
} from '@sap/cds/common';
using {flj.common as common} from './common';

namespace flj.core;

type SizeCategory : String(2) enum {
    XL = 'Extra Large';
    L  = 'Large';
    M  = 'Medium';
    S  = 'Small'
}

type Meals        : many {
    name : String;
    size : SizeCategory;
}

entity Bookings : cuid {
    // associations
    customer  : Association to Customers @mandatory;
    // customer_ID : UUID;
    // customer    : Association to Customers
    //                   on customer.ID = customer_ID;
    flight    : Association to Flights   @mandatory;
    // flight_ID   : UUID;
    // flight      : Association to Flights
    //                   on flight.ID = flight_ID;
    // properties
    smoker    : Boolean;

    @Measures.Unit       : wunit
    luggweigh : Decimal(8, 4);
    wunit     : common.Unit;
    class     : common.Class;

    @Measures.ISOCurrency: forcurkey_code
    forcuram  : Decimal(9, 2);
    forcurkey : Currency;

    @Measures.ISOCurrency: loccurkey_code
    loccuram  : Decimal(9, 2);
    loccurkey : Currency;
    order_dat : Date;
    cancelled : Boolean;
    reserved  : Boolean;
    passname  : String;
    passform  : String;
    passbirt  : String;
}

entity Customers : cuid, common.contact {
    // properties
    form  : String;
    name  : String;
    langu : Language;
    meals : Meals;
}

entity Flights : cuid {
    // associations
    bookings      : Association to many Bookings
                        on bookings.flight = $self;
    connection_ID : common.Connid;
    connection    : Composition of Connections
                        on connection.connection_ID = connection_ID;
    // properties
    fldate        : Date;

    @Measures.ISOCurrency: currency_code
    price         : Decimal(9, 2);
    currency      : Currency;
    planetype     : String;

    @Measures.ISOCurrency: currency_code
    paymentsum    : Decimal(9, 2);
}

extend Flights with {
    seatsmax           : Integer;
    seatsocc           : Integer;
    seatsmax_b         : Integer;
    seatsocc_b         : Integer;
    seatsmax_f         : Integer;
    seatsocc_f         : Integer;
    virtual seatsmax_e : Integer;
    virtual seatsocc_e : Integer;
}

entity Connections {
        // keys
    key connection_ID : common.Connid;
        // associations
        flights       : Association to many Flights
                            on flights.connection = $self;
        carrier_ID    : common.Carrid;
        carrier       : Composition of Carriers
                            on carrier.carrier_ID = carrier_ID;
        // properties
        countryfr     : Country;
        cityfrom      : String;
        airpfrom      : String;
        countryto     : Country;
        cityto        : String;
        airpto        : String;
        fltime        : Time;
        deptime       : Time;
        arrtime       : Time;
        distance      : Decimal(9, 2);
        dunit         : common.Unit;
        fltype        : String(1);
        period        : Integer;
}

entity Carriers {
        // keys
    key carrier_ID  : common.Carrid;
        // associations
        connections : Association to many Connections
                          on connections.carrier = $self;
        // properties
        name        : String;
        currcode    : Currency;
        url         : String;
}
