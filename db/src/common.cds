using {
    cuid,
    Country,
    Currency,
    Language,
    sap.common.CodeList
} from '@sap/cds/common';

namespace flj.common;

type Connid       : String(2);
type Carrid       : String(4);
type Unit         : String(3);

// type Class        : String(1) enum {
//     F  = 'First Class';
//     C  = 'Business Class';
//     Y  = 'Economy Class';
// }

type Class        : Association to ClassTypes;

entity ClassTypes : CodeList {
    key code  : String(1);
    // name  : localized String(255)  @title: '{i18n>Name}';
    // descr : localized String(1000) @title: '{i18n>Description}';
}

aspect address {
    street   : String;
    postbox  : String;
    postcode : String;
    city     : String;
    country  : Country;
}

aspect contact : address {
    telephone : String;
    email     : String;
    webuser   : String;
}