const cds = require('@sap/cds')

module.exports = {
    readFlights: async (req, next) => {
        // 1. cql statement
        // let query = cds.parse.cql(`SELECT from ${req.target.name}`)
        // let result = await cds.tx(req).run(query)

        // 2. query object
        // let query = SELECT.from(req.target)
        // let result = await cds.tx(req).run(query)

        // 3. query object wihtout cds.tx(req).run
        // let result = await SELECT.from(req.target)

        // 4. using query from request
        // let result = await cds.tx(req).run(req.query)

        // clean solution using next
        let result = await next()
        if (!Array.isArray(result)) {
            result.seatsmax_e = result.seatsmax - result.seatsmax_b - result.seatsmax_f
            result.seatsocc_e = result.seatsocc - result.seatsocc_b - result.seatsocc_f
        } else {
            result.forEach((r) => {
                r.seatsmax_e = r.seatsmax - r.seatsmax_b - r.seatsmax_f
                r.seatsocc_e = r.seatsocc - r.seatsocc_b - r.seatsocc_f
            })
        }
        return result
    },

    updateFlights: async (req, next) => {
        let entity = await SELECT.one.from(req.target).where({ID: req.data.ID})

        let tmpData = req.data
        if (tmpData.seatsmax_f) {
            tmpData.seatsmax = entity.seatsmax - (entity.seatsmax_f - tmpData.seatsmax_f)
        }
        if (tmpData.seatsmax_b) {
            tmpData.seatsmax = entity.seatsmax - (entity.seatsmax_b - tmpData.seatsmax_b)
        }

        // 1. query object
        // let query = UPDATE.entity(req.target).where({ID: req.data.ID}).data(tmpData)
        // await cds.tx(req).run(query)
        
        // 2. query object wihtout cds.tx(req).run
        // await UPDATE.entity(req.target).where({ID: req.data.ID}).data(tmpData)

        // 3. using query from request
        // req.query.data(tmpData)
        // await cds.tx(req).run(req.query)

        req.data = tmpData
        await next()

        // return updated entry
        let result = await SELECT.one.from(req.target).where({ID: req.data.ID})
        return result
    },

    createBookings: async (req, next) => {
        // 1. query object
        // let query = INSERT.into(req.target).entries([{
        //     ...req.data,
        //     order_dat: '2021-01-01'
        // }])
        // await cds.tx(req).run(query)
        
        // 2. query object wihtout cds.tx(req).run
        // await INSERT.into(req.target).entries([{
        //     ...req.data,
        //     order_dat: '2021-01-01'
        // }])

        // 3. using query from request
        // req.query.entries([{
        //     ...req.data,
        //     order_dat: '2021-01-01'
        // }])
        // await cds.tx(req).run(req.query)

        // clean solution using next
        req.data.order_dat = new Date().toISOString().substring(0,10)
        await next()

        // return created entry
        let result = await SELECT.one.from(req.target).where({ID: req.data.ID})
        return result
    },
    
    setIsSmoker: async (req, next) => {
        const booking = await cds.tx(req).run(req.query)
        const bookings = Array.isArray(booking) ? booking : [booking] 

        for (let i = 0; i < bookings.length; i++) {
            const b = bookings[i]
            let query = UPDATE.entity(req.target).where({ID: b.ID}).data(req.data)
            await cds.tx(req).run(query)
        }

        // return updated entries
        let result = await cds.tx(req).run(req.query)
        return Array.isArray(result) && result.length === 1 ? result.at(0) : result
    },
}