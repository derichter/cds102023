using {flj.core as core} from '../db/schema.cds';

service AdminService @(requires: 'authenticated-user') { // @(requires: 'admin')
    entity Flights     as projection on core.Flights;

    entity Bookings    as projection on core.Bookings actions {
        action setIsSmoker(smoker : Boolean) returns Bookings;
    }

    entity Customers   as projection on core.Customers;
    
    entity Connections as projection on core.Connections;

    entity Carriers    as projection on core.Carriers;
}
