const cds = require('@sap/cds')

const { 
    readFlights,
    updateFlights,
    createBookings,
    setIsSmoker,
} = require('./utility/handler')

module.exports = function (){
    this.on('READ', `Flights`, readFlights) 

    this.on('UPDATE', `Flights`, updateFlights)

    // this.on('CREATE', `Flights`, () => {})

    // this.on('READ', `Bookings`, () => {}) 

    // this.on('UPDATE', `Bookings`, () => {})

    this.on('CREATE', `Bookings`, createBookings)

    this.on('setIsSmoker', `Bookings`, setIsSmoker)

    // this.before('CREATE', `Bookings`, async (req, next) => {
    //     console.log()
    // })
    // this.on('CREATE', `Bookings`, async (req, next) => {
    //     console.log()
    //     next()
    // })
    // this.on('CREATE', `Bookings`, async (req, next) => {
    //     console.log()
    // })
    // this.after('CREATE', `Bookings`, async (req, next) => {
    //     console.log()
    // })
}