const cds = require('@sap/cds')

const { 
    readFlights,
    updateFlights,
    createBookings,
    setIsSmoker,
} = require('./utility/handler')

module.exports = function (){
    this.on('READ', `Flights`, readFlights) 
    this.on('UPDATE', `Flights`, updateFlights)
    this.on('CREATE', `Bookings`, createBookings)
    this.on('setIsSmoker', setIsSmoker)
}