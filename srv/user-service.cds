using { flj.core as core } from '../db/schema.cds';

service UserService @(requires: 'authenticated-user') {
    @readonly
    entity Flights as projection on core.Flights;
    @readonly
    entity Bookings as projection on core.Bookings;
    @readonly
    entity Customers as projection on core.Customers;
    @readonly
    entity Connections as projection on core.Connections;
    @readonly
    entity Carriers as projection on core.Carriers;
}

